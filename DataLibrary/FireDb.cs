﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;

namespace DataLibrary
{
    public class FireDb
    {
        FbConnection cn;
        public FireDb(string conn )
        {
            string cn_ = "ServerType=1;User=SYSDBA;" +
                 @"Password=masterkey;Dialect=3;Database=fire_db.fdb;ClientLibrary=d:\fb\fbembed.dll";
            if (conn == null) conn = cn_;
            cn = new FbConnection(conn);
            //createDbFill();
           
            
        }
        FbCommand cmd;
        public List<string[]> getData(string qry,int col)
        {
            cmd.CommandText = qry;
           var read = cmd.ExecuteReader();
            var tb = new List<string[]>();
            while (read.Read())
            {
                var arr = new string[col];
                for (var i = 0; i < col; i++)
                   arr[i] = read[i].ToString();
                tb.Add(arr);
            }
            return tb;
        }
        public void createDbFill(string conn) {
            FbConnection.CreateDatabase(conn, true);
            cn.Open();
            cmd = new FbCommand(@"reCREATE TABLE Forecast
   (
  dt CHAR (24),
  city varCHAR(100),
  temp numeric(5,2) );", cn);
            cmd.ExecuteNonQuery();
            cmd.CommandText = @"INSERT INTO forecast 
VALUES ('25.12.19', 'Krsk', -19.08);";
            cmd.ExecuteNonQuery();
        }
    }
}
