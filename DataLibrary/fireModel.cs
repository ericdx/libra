namespace dataLibrary
{
    using FirebirdSql.Data.FirebirdClient;
    using Microsoft.EntityFrameworkCore;
    // using System.Data.SqlServerCe;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    //using System.Data.Entity;
    using System.Linq;
    /// <summary>
    /// fb + ef , not embedded!
    /// </summary>
    public class compactModel : DbContext
    {
       
        public compactModel()
            //: base(new FbConnection(@"database=localhost:fb_db.fdb;user=sysdba;password=masterkey"))
        {
        }
        string conn = @"database=127.0.0.1:fb_db.fdb;user=sysdba;password=masterkey";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseFirebird(conn);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var demoConf = modelBuilder.Entity<ConnectionEntity>();
            demoConf.Property(x => x.Name).HasColumnName("ID");
            demoConf.Property(x => x.lastUsed).HasColumnName("FOOBAR");
            demoConf.ToTable("DEMO");
        }
        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<ConnectionEntity> connEntities { get; set; }
    }

    public class ConnectionEntity
    {
        [Key]
       /* [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }*/
        public string Name { get; set; }
public bool lastUsed { get; set; }
    }
}