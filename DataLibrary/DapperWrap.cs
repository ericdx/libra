﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using FirebirdSql.Data.FirebirdClient;

namespace Data
{
    public class DapperWrap
    {
        private string conn;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_conn">tiss db if null</param>
        public DapperWrap(string _conn)
        {
           
                conn = _conn;
        }


        public HashSet<string> getColumn(string qry) //column must be A
        {
            var lst = new HashSet<string>();
            using (IDbConnection db =  provideConn(conn))
            {
                var rw = db.Query(qry);
                foreach (var r in rw)
                {
                    lst.Add(r.A);
                }
            }
            return lst;
        }
        IDbConnection provideConn(string conn) {
            return new FbConnection(conn);
        }
        public List<KeyValuePair<string,string>> get2Columns(string qry,object par = null) //columns must be A,B
        {
            var lst = new List<KeyValuePair<string,string>>();

            using (IDbConnection db =  provideConn(conn))
            {
                var rw = db.Query(qry,par);
                foreach (var r in rw)
                {

                    lst.Add(new KeyValuePair<string, string> (r.A,r.B));
                }
            }
            return lst;
        }
        public void insertTiming(int[] arr)
        {
            var cmd = "INSERT INTO [log].[block_timings] VALUES (@sid,@op,@time,@ordr)";
            var ins = new {sid = arr[2], op = arr[1], time = arr[0], ordr = arr[3]};
            if (ins.time < 300000) //5 min in ms
            using (IDbConnection db = new FbConnection(conn))
            {
                db.Execute(cmd, ins);
            }
        }
        public void insertCounter(string user)
        {
            var cmd = @"
INSERT INTO [dbo].[Daily_Api_Count]
           ([User_Name]
           )
     VALUES
           (@user)";
            var ins = new { user = user};
            
                using (IDbConnection db = new FbConnection(conn))
                {
                try
                {
                    db.Execute(cmd, ins);
                } catch { } //may exist
                }
        }
        public void insertToken(string user,string token,string email)
        {
            var cmd1 = @" delete from [dbo].[ApiTokens] where userCode = '" + user + "'";
            var cmd2 = @"
INSERT INTO [dbo].[ApiTokens]
           ([userCode]
           ,[token]           
           ,[sentToEmail])
     VALUES
           (@usr
           ,@token
           ,@email)";
            var ins = new { usr = user, token = token, email = email};
            
                using (IDbConnection db = new FbConnection(conn))
                {
                db.Execute(cmd1);
                db.Execute(cmd2, ins);
                }
        }
        public DataTable getTable(string qry)
        {
            var table = new DataTable();

            using (var con = new FbConnection(conn))
            using (var cmd = new FbCommand(qry, con))
            using (var da = new FbDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return table;
        }

        public void updatePass(string code, string pwd, string hash)
        {
            var cmd = "update [dbo].[AspNetUsers] set passwordhash = @hash,[PasswordText]=@pwd where username=@user";
            var ins = new { hash = hash,user = code, pwd = pwd};            
                using (IDbConnection db = new FbConnection(conn))
                {
                    db.Execute(cmd, ins);
                }
        }
    }
}
