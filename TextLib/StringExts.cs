﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib
{
   public static class StringExts
    {
        /// <summary>
        /// to write str.IsEmptyNull() instead of string.IsNullOrEmpty(str)
        /// </summary>
        /// <param name="source">string to check</param>
        /// <returns></returns>
        public static bool IsEmptyNull(this string source)
        {
            return string.IsNullOrEmpty(source);
        }
        public static bool equalAnycaseTrim(this string str1, string str2)
        {
            str1 = str1?.Trim();
            str2 = str2?.Trim();
            return string.Compare(str1, str2, true) == 0;
        }
    }
}
