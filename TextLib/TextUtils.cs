﻿/*
 * Created by SharpDevelop.
 * User: Эрнест
 * Date: 14.07.2019
 * Time: 0:25
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace TextLib
{
    /// <summary>
    /// 
    /// </summary>
    public class TextUtils
    {
        public static string filesFolder = "config";
        public static void save(object item, string fname)
        {
            Directory.CreateDirectory(filesFolder);
            File.WriteAllBytes(Path.Combine(filesFolder, fname), ObjectToByteArray(item));
        }

        public static T tryRestore<T>(string file)
        {
            return (T)load(file);
        }

        public static object load(string fname)
        {
            var path = Path.Combine(filesFolder, fname);
            if (File.Exists(path))
                return bytesToObject(File.ReadAllBytes(path));
            return null;
        }
        static byte[] ObjectToByteArray(object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }
        static object bytesToObject(byte[] arr)
        {
            if (arr == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream(arr))
            {
                return bf.Deserialize(ms);
            }
        }
    }
}