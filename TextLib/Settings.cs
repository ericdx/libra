﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextLib
{
   public class Settings
    {
        public static string[] Get(string key)
        {
            return ConfigurationSettings.AppSettings[key]?.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        }
        public static void set(string setting, object value)
        {
            File.WriteAllText("setting_" + setting, value.ToString());
        }
        public static string getFile(string setting)
        {
            if (!File.Exists("setting_" + setting))
                return "false";
           return File.ReadAllText("setting_" + setting);
        }
        public static bool fromFile(string fl,out string value)
        {
            value = null;
            if (!File.Exists(fl))
                return false;
           value = File.ReadAllText(fl);
            return true;
        }
    }
}
