﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsLibrary
{
    public static class FormsExt
    {
        /// <summary>
        /// remove items. Returns cleared
        /// </summary>
        /// <param name="items"></param>
        /// <param name="substr"></param>
        public static string[] removeItemsContaining(this ListBox.ObjectCollection items, string substr)
        {
            var removed = new List<string>();
            for (var i=0;i<items.Count;)
            {
                var item = (string) items[i];
                if (item.Contains(substr))
                {
                    removed.Add(item);
                    items.RemoveAt(i);
                }
                else i++;
            }
            return removed.ToArray();
        }
        public static void refill(this ListBox.ObjectCollection items, string[] fill)
        {
            items.Clear(); items.AddRange(fill);
        }
    }
}
