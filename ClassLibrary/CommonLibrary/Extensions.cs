﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    public static class Extensions
    {
        /// <summary>
        /// only adds new keys
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="dic"></param>
        /// <param name="dic2"></param>
        public static void MergeDictionaries<T, T2>(this Dictionary<T, T2> dic,Dictionary<T, T2> dic2 )
        {
            if (dic2 == null) return;
            foreach (var item in dic2)
            {
                if (!dic.ContainsKey(item.Key))
                {
                    dic.Add(item.Key,item.Value);
                }
            }
        }
        public static bool ContainsAnycase(this string source, string toCheck)
        {
            if (source == null) return false;
            return source.IndexOf(toCheck, StringComparison.OrdinalIgnoreCase) >= 0;
        }
       public static byte[] toByteArray(this object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }
    }
}
