﻿using PushbulletSharp;
using PushbulletSharp.Models.Requests;
using PushbulletSharp.Models.Responses;
using System;
using System.Collections.Generic;
using TextLib;
//using ClassLib;
//using SettingsLibrary;

namespace AlertLibrary
{
    public class Notifier
    {
        public static void alarm(string msg,string title = "noname")
        {
            alertMesg(msg, title);
        }
        public static void alertTiss(string msg)
        {
            alertMesg(msg, "apz alert");
        }
        public static void alertMesg(string msg, string title)
        {
            string key,recipient;
            Settings.fromFile("pb_key.txt", out key);
            Settings.fromFile("pb_to.txt", out recipient);
            PushbulletClient client = new PushbulletClient(key);

            //If you don't know your device_iden, you can always query your devices
            var devices = client.CurrentUsersDevices();

          /*  var device = devices.Devices.Single(d => d.Iden == "");//ernest

            if (device != null)*/
            
                PushNoteRequest request = new PushNoteRequest
                { //Email = "ericdx@rambler.ru",
                    DeviceIden = recipient, //BQ
                                      
                    Title = title,
                    Body = msg
                };

                //PushResponse response = 
                if (  request.Body != null)
                try
                {
                 var resp = client.PushNote(request);            
                }
                catch (Exception ex)
                {
                    throw;
                    //Logger.logFile(ex.Message);                       
                }
                   
        }

        public static void alertByClient(string msg, string token, string title = "none")
        {
            PushbulletClient client = new PushbulletClient(token);

                PushNoteRequest request = new PushNoteRequest
                {                 
                    Title = title,
                    Body = msg
                };

                PushResponse response = client.PushNote(request);
               
        }

        public static void alarm(string message, string title,ref int current, int max)
        {
            if (current > max)
                return;

                alarm(message, title);
            current++;
        }
    }
}
