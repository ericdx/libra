﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;


namespace Logic.Services
{
    public class MailSenderService 
    {    
       
        
        public bool SendMailTo(string fromEmail, string subject, string body, string userName, string toEmail)
        {
            try
            {
                var emails = fromEmail.Split(new char[] {',', ';', ' '}, StringSplitOptions.RemoveEmptyEntries);
                if (emails.Length > 0)
                    fromEmail = emails[0];

              
                var bodyText = body + System.Environment.NewLine + "сообщение с сайта ";

                if (string.IsNullOrEmpty(bodyText) || string.IsNullOrEmpty(subject) || string.IsNullOrEmpty(toEmail))
                    return false;
                string mailCfg;
                TextLib.Settings.fromFile("mail", out mailCfg);
                var cfg = mailCfg.Split(';');
                var senderAddr = cfg[0];
                var senderPass = cfg[1];
                var mailServer = cfg[2];
                var message = new MailMessage();

                message.From = new MailAddress(fromEmail);
                message.Subject = subject;
                message.Body = bodyText;
                message.To.Add(toEmail);
                
                using (var smtp = new SmtpClient(mailServer, 25))
                {
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(senderAddr, senderPass);
                   
                        smtp.Send(message);
                        return true;
                   
                }
            }
            catch (Exception ex)
            {
               
               //     Logger.dbInstance.Error($"email about '{subject}' not sent to {toEmail}", ex);
            }
            return false;
        }
               
    }
}
