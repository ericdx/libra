﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using VirusTotalNet;
using VirusTotalNet.ResponseCodes;
using VirusTotalNet.Results;

namespace NetLibrary
{
   public class TotalScan
    {
        string key;
        public TotalScan(string api)
        {
            key = api;
        }
                public  bool checkFile(string path, out bool alert)
        {
            alert = false;
            try
            {
                var fi = new FileInfo(path);

                FileReport fileReport = (new VirusTotal(key)).GetFileReportAsync(fi).Result;//TestData.EICARMalware);
                alert = fileReport.Positives > 0;
                return fileReport.Positives == 0 && fileReport.ResponseCode == FileReportResponseCode.Present;
            } catch
            {
                return false;
            }
        }
    }
}
