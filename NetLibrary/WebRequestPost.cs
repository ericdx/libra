﻿using CommonLibrary;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Net
    {
        public  class WebRequestPost
        {
        public WebRequestPost(string baseUri)
        {
            _base = baseUri;
        }

        string _base;

        public string Post(string uri, object data = null, Dictionary<string, string> get_args = null)
        {
            var par = "";
            if (get_args != null)
            {
                par = "?";
                foreach (var pm in get_args)
                {
                    par += $"{pm.Key}={pm.Value}&";
                }
                par = par.Substring(0, par.Length - 1);
            }
            var url = _base + uri + par;
            // Create a request using a URL that can receive a post.   
            WebRequest request = WebRequest.Create(url);


            // Set the Method property of the request to POST.  
            request.Method = data == null ? "GET" : "POST";

            // Create POST data and convert it to a byte array.  
            if (data != null)
            { 
            byte[] byteArray = data.toByteArray();

            // Set the ContentType property of the WebRequest. 
            if (byteArray != null)
                request.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.  
            request.ContentLength = byteArray?.Length ?? 0;

            // Get the request stream.  
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.  
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.  
            dataStream.Close();
        }
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
               

                // Get the stream containing content returned by the server.  
                // The using block ensures the stream is automatically closed.
                using (var dataStream = response.GetResponseStream())
                {
                    // Open the stream using a StreamReader for easy access.  
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.  
                    string responseFromServer = reader.ReadToEnd();
                // Display the content.  
                return responseFromServer;
                }

                // Close the response.  
                response.Close();
            return null;
            }
        }
    }

